﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace PathOfMapsClient.HelperClasses {
    class POELogWatcher {
        private const char COMMAND_CHAR = '>';
        private const string START_MAP_REGEX = @"(?:(name)\s(.+ Map))|(?:(iq|ir|q|ps|rarity|craft)\s(\d{1,3}|\S+))";
        private long previousSize = 0;
        private bool watchFiles = false;

        private MainForm mainForm;

        public POELogWatcher(MainForm mainForm) {
            this.mainForm = mainForm;
            if (String.IsNullOrWhiteSpace(MainForm.config.client_txt_location)) {
                MainForm.config.client_txt_location = FindClientTxtPath();
            }
        }

        private string FindClientTxtPath() {
            Environment.SpecialFolder[] folders = { Environment.SpecialFolder.ProgramFiles,
                Environment.SpecialFolder.ProgramFilesX86,
                Environment.SpecialFolder.CommonProgramFiles,
                Environment.SpecialFolder.CommonProgramFilesX86 };

            foreach (var folder in folders) {
                try {
                    if (Directory.Exists(Environment.GetFolderPath(folder))) {
                        string clientPath = Path.Combine(Environment.GetFolderPath(folder),
                            @"Grinding Gear Games\Path of Exile\logs\Client.txt");

                        if (File.Exists(clientPath)) {
                            return clientPath;
                        }
                    }
                }
                catch (Exception) {
                    // Todo: log?
                }
            }

            string steamPoeClientTxt = FindClientTxtPathForSteam();
            if (!String.IsNullOrWhiteSpace(steamPoeClientTxt)) {
                return steamPoeClientTxt;
            }

            return "";
        }

        private string FindClientTxtPathForSteam() {
            try {
                RegistryKey regKey = Registry.CurrentUser;
                regKey = regKey.OpenSubKey(@"Software\Valve\Steam");

                if (regKey != null) {
                    string steamPath = regKey.GetValue("SteamPath").ToString();
                    string poeFolder = Path.Combine(steamPath, "steamapps/common/Path of Exile");
                    string poeClientTxt = Path.Combine(poeFolder, "Client.txt");
                    if (Directory.Exists(poeFolder) && File.Exists(poeClientTxt)) {
                        return poeClientTxt;
                    }
                }
            }
            catch (Exception) {
                // Todo: log?
            }

            return "";
        }

        public delegate void CommandHandler(string log);

        public void StartWatching(CommandHandler handlerFunc) {
            if (!String.IsNullOrWhiteSpace(MainForm.config.client_txt_location)) {
                if (!watchFiles) {
                    watchFiles = true;
                    new Thread(() => {
                        while (watchFiles) {
                            try {
                                using (Stream stream = File.Open(MainForm.config.client_txt_location, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
                                    using (StreamReader streamReader = new StreamReader(stream)) {
                                        if (previousSize == 0) {
                                            previousSize = streamReader.BaseStream.Length;
                                        }
                                        else if (previousSize < streamReader.BaseStream.Length) {
                                            streamReader.BaseStream.Seek(previousSize, SeekOrigin.Begin);
                                            string data = streamReader.ReadToEnd();
                                            //Console.WriteLine(data);
                                            previousSize = streamReader.BaseStream.Length;
                                            streamReader.Close();
                                            String[] commands = GetCommands(data);
                                            foreach (string command in commands) {
                                                handlerFunc(command);
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception) {
                                // Todo: log?
                                //Console.WriteLine(ex.Message);
                            }

                            Thread.Sleep(MainForm.config.log_read_interval);
                        }
                    }).Start();
                }
            }
        }

        public void StopWatching() {
            watchFiles = false;
        }

        private String[] GetCommands(string log) {
            List<string> commands = new List<string>();
            String[] loglines = log.Replace("\r", "").Split('\n');
            foreach (string line in loglines) {
                Match regex = Regex.Match(line,
                                    @"\[INFO Client \d+\]\s" + Regex.Escape(MainForm.config.character_name) + @":\s" + COMMAND_CHAR + @"(.+)?", RegexOptions.IgnoreCase);
                if (regex.Success) {
                    commands.Add(regex.Groups[1].Value);
                }
            }
            return commands.ToArray();
        }
    }
}