﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Windows.Forms;

namespace PathOfMapsClient.HelperClasses {
    class InternalMobileAPI {
        private MainForm mainForm;

        public InternalMobileAPI(MainForm mainForm) {
            this.mainForm = mainForm;
        }

        public string HandleWebRequests(HttpListenerRequest request) {
            if (request.Url.Segments.Length < 3) { // Does not contain REST action
                return "";
            }

            string action = request.Url.Segments[2];
            switch (action) {
                case "get-current-map":
                    return GetCurrentMap(request);
                case "end-map":
                    return EndMap(request);
                case "found-master":
                    return FoundMaster(request);
                case "add-zana-mod":
                    return AddZanaMod(request);
            }

            Console.WriteLine(String.Join(", ", request.Url.Segments));

            return "HOI";
        }

        private string GetCurrentMap(HttpListenerRequest request) {
            return "";
            //return JsonConvert.SerializeObject(mainForm.CurrentMap, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
        }

        private string EndMap(HttpListenerRequest request) {
            mainForm.Invoke((MethodInvoker)delegate () {
                //mainForm.EndMap();
            });
            return "";
        }

        private string FoundMaster(HttpListenerRequest request) {
            mainForm.Invoke((MethodInvoker)delegate () {
                //mainForm.EndMap();
            });
            return "";
        }

        private string AddZanaMod(HttpListenerRequest request) {
            mainForm.Invoke((MethodInvoker)delegate () {
                //mainForm.EndMap();
            });
            return "";
        }
    }
}
