﻿using System;
using System.Windows.Forms;

namespace PathOfMapsClient {
    public partial class ZanaModForm : Form {
        public ZanaModForm() {
            InitializeComponent();
        }

        public enum ZanaMods {
            NO_MOD,
            ONSLAUGHT,
            RAMPAGE,
            ANARCHY,
            BLOODLINES,
            TORMENT,
            WARBANDS,
            BEYOND,
            AMBUSH,
            TEMPEST,
            DOMINATION,
            NEMESIS,
            PERANDUS,
            BREACH
        }

        public ZanaMods ZanaMod { get; set; }

        private void btnZanaModPicked(object sender, EventArgs e) {
            switch (((Control)sender).Name) {
                case "btnOnslaught":
                    ZanaMod = ZanaMods.ONSLAUGHT;
                    break;
                case "btnBloodlines":
                    ZanaMod = ZanaMods.BLOODLINES;
                    break;
                case "btnBeyond":
                    ZanaMod = ZanaMods.BEYOND;
                    break;
                case "btnAmbush":
                    ZanaMod = ZanaMods.AMBUSH;
                    break;
                case "btnPerandus":
                    ZanaMod = ZanaMods.PERANDUS;
                    break;
                case "btnNemesis":
                    ZanaMod = ZanaMods.NEMESIS;
                    break;
                case "btnBreach":
                    ZanaMod = ZanaMods.BREACH;
                    break;
                case "btnNoMod":
                default:
                    ZanaMod = ZanaMods.NO_MOD;
                    break;
            }
            this.Close();
        }
    }
}
