﻿namespace PathOfMapsClient {
    partial class ZanaModForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnBreach = new PathOfMapsClient.FutureButton();
            this.btnNemesis = new PathOfMapsClient.FutureButton();
            this.btnNoMod = new PathOfMapsClient.FutureButton();
            this.btnPerandus = new PathOfMapsClient.FutureButton();
            this.btnAmbush = new PathOfMapsClient.FutureButton();
            this.btnBeyond = new PathOfMapsClient.FutureButton();
            this.btnBloodlines = new PathOfMapsClient.FutureButton();
            this.btnOnslaught = new PathOfMapsClient.FutureButton();
            this.SuspendLayout();
            // 
            // btnBreach
            // 
            this.btnBreach.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnBreach.Location = new System.Drawing.Point(124, 112);
            this.btnBreach.Name = "btnBreach";
            this.btnBreach.Size = new System.Drawing.Size(106, 44);
            this.btnBreach.TabIndex = 7;
            this.btnBreach.Text = "Breach (6c)";
            this.btnBreach.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnNemesis
            // 
            this.btnNemesis.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnNemesis.Location = new System.Drawing.Point(124, 62);
            this.btnNemesis.Name = "btnNemesis";
            this.btnNemesis.Size = new System.Drawing.Size(106, 44);
            this.btnNemesis.TabIndex = 6;
            this.btnNemesis.Text = "Nemesis (5c)";
            this.btnNemesis.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnNoMod
            // 
            this.btnNoMod.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnNoMod.Location = new System.Drawing.Point(12, 212);
            this.btnNoMod.Name = "btnNoMod";
            this.btnNoMod.Size = new System.Drawing.Size(218, 44);
            this.btnNoMod.TabIndex = 5;
            this.btnNoMod.Text = "None";
            this.btnNoMod.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnPerandus
            // 
            this.btnPerandus.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnPerandus.Location = new System.Drawing.Point(124, 12);
            this.btnPerandus.Name = "btnPerandus";
            this.btnPerandus.Size = new System.Drawing.Size(106, 44);
            this.btnPerandus.TabIndex = 4;
            this.btnPerandus.Text = "Perandus (4c)";
            this.btnPerandus.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnAmbush
            // 
            this.btnAmbush.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnAmbush.Location = new System.Drawing.Point(12, 162);
            this.btnAmbush.Name = "btnAmbush";
            this.btnAmbush.Size = new System.Drawing.Size(106, 44);
            this.btnAmbush.TabIndex = 3;
            this.btnAmbush.Text = "Ambush (4c)";
            this.btnAmbush.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnBeyond
            // 
            this.btnBeyond.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnBeyond.Location = new System.Drawing.Point(12, 112);
            this.btnBeyond.Name = "btnBeyond";
            this.btnBeyond.Size = new System.Drawing.Size(106, 44);
            this.btnBeyond.TabIndex = 2;
            this.btnBeyond.Text = "Beyond (3c)";
            this.btnBeyond.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnBloodlines
            // 
            this.btnBloodlines.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnBloodlines.Location = new System.Drawing.Point(12, 62);
            this.btnBloodlines.Name = "btnBloodlines";
            this.btnBloodlines.Size = new System.Drawing.Size(106, 44);
            this.btnBloodlines.TabIndex = 1;
            this.btnBloodlines.Text = "Bloodlines (3c)";
            this.btnBloodlines.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // btnOnslaught
            // 
            this.btnOnslaught.Font = new System.Drawing.Font("Verdana", 8F);
            this.btnOnslaught.Location = new System.Drawing.Point(12, 12);
            this.btnOnslaught.Name = "btnOnslaught";
            this.btnOnslaught.Size = new System.Drawing.Size(106, 44);
            this.btnOnslaught.TabIndex = 0;
            this.btnOnslaught.Text = "Onslaught (2c)";
            this.btnOnslaught.Click += new System.EventHandler(this.btnZanaModPicked);
            // 
            // ZanaModForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.ClientSize = new System.Drawing.Size(247, 269);
            this.Controls.Add(this.btnBreach);
            this.Controls.Add(this.btnNemesis);
            this.Controls.Add(this.btnNoMod);
            this.Controls.Add(this.btnPerandus);
            this.Controls.Add(this.btnAmbush);
            this.Controls.Add(this.btnBeyond);
            this.Controls.Add(this.btnBloodlines);
            this.Controls.Add(this.btnOnslaught);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ZanaModForm";
            this.Text = "Add Zana mod";
            this.ResumeLayout(false);

        }

        #endregion

        private FutureButton btnOnslaught;
        private FutureButton btnBloodlines;
        private FutureButton btnBeyond;
        private FutureButton btnAmbush;
        private FutureButton btnPerandus;
        private FutureButton btnNoMod;
        private FutureButton btnNemesis;
        private FutureButton btnBreach;
    }
}