﻿//#define WEBSERVER_ENABLED
#define LOGWATCHER_ENABLED

using System;
using System.Threading;
using System.Windows.Forms;
using System.Media;
using Newtonsoft.Json;
using System.IO;
using PathOfMapsClient.HelperClasses;
using System.Drawing;
using PathOfMapsClient.Forms;
using PathOfMapsClient.Models;
using System.Linq;
using System.Diagnostics;

namespace PathOfMapsClient {
    public partial class MainForm : Form {
        #region PRIVATES
        private static KeyboardHook KeyboardHook = new KeyboardHook();
        private static SettingsForm settingsForm = new SettingsForm();
        private static readonly string CONFIG_PATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Path of Maps\\pathofmaps_config.json");
        private static readonly string OLD_CONFIG_PATH = Path.Combine(Directory.GetCurrentDirectory(), "pathofmaps_config.json");

#if WEBSERVER_ENABLED
        private WebServer WS = null;
        private InternalMobileAPI MobileAPI = null;
#endif

#if LOGWATCHER_ENABLED
        private POELogWatcher watcher = null;
#endif

        public static ConfigObj config = new ConfigObj();
        public PathOfMapsAPI API = new PathOfMapsAPI();
        #endregion PRIVATES

        #region FORM_EVENTS
        public MainForm() {
            InitializeComponent();
            KeyboardHook.KeyDown += KeyboardHook_KeyDown;
            this.ActiveControl = lblCurrentMapRunning;
            ReadConfigIntoUI();

#if LOGWATCHER_ENABLED
            watcher = new POELogWatcher(this);
            watcher.StartWatching(LogCommandHandler);
#endif

#if WEBSERVER_ENABLED
            MobileAPI = new InternalMobileAPI(this);
            WS = new WebServer(new string[] { "http://*:8080/pathofmaps-api/" }, MobileAPI.HandleWebRequests);
            WS.Run();

            WebServer.AddProgramToFirewall();
#endif
        }

        private async void MainForm_Shown(object sender, EventArgs e) {
            if (String.IsNullOrWhiteSpace(config.api_token) || String.IsNullOrWhiteSpace(config.character_name)) {
                MessageBox.Show("Welcome to Path of Maps!\n\n" +
                    "Before you can log your maps and drops, " +
                    "please enter your API token and character name in the settings screen.\n\n" +
                    "The settings screen will open after this message.", "Welcome to Path of Maps!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ShowSettings();
            }

            UpdateObj update = await Updater.CheckForUpdates();
            if (update.releases != null && update.releases.Count > 0) {
                Release release = update.releases.OrderByDescending(rl => DateTime.Parse(rl.created_at)).First();
                DialogResult result = MessageBox.Show("There is an update available! Version " + release.version +
                        " can be downloaded.\n\nWould you like to visit the website to download the latest version?",
                        "Update available", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes) {
                    Process.Start(release.download_url);
                }
            }
        }

        private bool KeyboardHook_KeyDown(int vkCode) {
            if (!settingsForm.Visible) {
                if ((((Keys)vkCode) == config.start_map_key) &&
                    (!config.start_map_key_alt || (config.start_map_key_alt && PoMUtils.AltPressed())) &&
                    (!config.start_map_key_shift || (config.start_map_key_shift && PoMUtils.ShiftPressed())) &&
                    (!config.start_map_key_ctrl || (config.start_map_key_ctrl && PoMUtils.CtrlPressed()))) {

                    PoMUtils.RunOnSTAThread(StartMap);

                    return false;
                }
                else if ((((Keys)vkCode) == config.log_drop_key) &&
                    (!config.log_drop_key_alt || (config.log_drop_key_alt && PoMUtils.AltPressed())) &&
                    (!config.log_drop_key_shift || (config.log_drop_key_shift && PoMUtils.ShiftPressed())) &&
                    (!config.log_drop_key_ctrl || (config.log_drop_key_ctrl && PoMUtils.CtrlPressed()))) {

                    PoMUtils.RunOnSTAThread(LogDrop);

                    return false;
                }
                else if ((((Keys)vkCode) == config.end_map_key) &&
                        (!config.end_map_key_alt || (config.end_map_key_alt && PoMUtils.AltPressed())) &&
                        (!config.end_map_key_shift || (config.end_map_key_shift && PoMUtils.ShiftPressed())) &&
                        (!config.end_map_key_ctrl || (config.end_map_key_ctrl && PoMUtils.CtrlPressed()))) {

                    PoMUtils.RunOnSTAThread(EndMap);
                    return false;
                }
            }

            return true;
        }

        private void LogCommandHandler(string command) {
            PoMAPIResponse response = API.SendCommandManually(">" + command);
            if (response.RequestSucceeded()) {
                this.Invoke((MethodInvoker)delegate () {
                    UpdateUIWithResponse(response);
                    SystemSounds.Exclamation.Play();
                });
            }
            else {
                ShowError("Could not execute the given command. Please try again!");
            }
        }

        private void btnEndMap_Click(object sender, EventArgs e) {
            if (MessageBox.Show("End the map?", "End map?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK) {
                PoMAPIResponse response = API.EndMap();
                if (response.RequestSucceeded()) {
                    UpdateUIWithResponse(response);
                    SystemSounds.Exclamation.Play();
                }
                else {
                    ShowError("Could not end map. Please try again.");
                }
            }
        }

        private void btnAbortMap_Click(object sender, EventArgs e) {
            if (MessageBox.Show("Abort the map?\n\nThis will delete the map with results.",
                "Abort map?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK) {
                PoMAPIResponse response = API.AbortMap();
                if (response.RequestSucceeded()) {
                    UpdateUIWithResponse(response);
                    SystemSounds.Exclamation.Play();
                }
                else {
                    ShowError("Could not abort map. Please try again.");
                }
            }
        }

        private void btnReopenMap_Click(object sender, EventArgs e) {
            if (MessageBox.Show("Reopen the last map?\n\nYou can then add drops to the map results.",
                "Reopen map?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK) {
                PoMAPIResponse response = API.ReOpenMap();
                if (response.RequestSucceeded()) {
                    UpdateUIWithResponse(response);
                    SystemSounds.Exclamation.Play();
                }
                else {
                    ShowError("Could not reopen map. Please try again.");
                }
            }
        }

        private void btnFoundMaster_Click(object sender, EventArgs e) {
            FoundMasterForm fmForm = new FoundMasterForm();
            fmForm.ShowDialog();
            FoundMasterForm.Masters master = fmForm.SelectedMaster;
            if (master == FoundMasterForm.Masters.NO_MASTER) {
                lblMaster.Text = "none";
                lblMaster.Font = new Font(lblMaster.Font, FontStyle.Regular);
                // Todo: clear master on API?
            }
            else {
                String masterName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(master.ToString().ToLower());
                lblMaster.Text = masterName;
                lblMaster.Font = new Font(lblMaster.Font, FontStyle.Bold);

                PoMAPIResponse response = API.FoundMaster(masterName.ToLower());
                if (response.RequestSucceeded()) {
                    UpdateUIWithResponse(response);
                    SystemSounds.Exclamation.Play();
                }
                else {
                    ShowError("Could not add master. Please try again.");
                }
            }
        }

        private void btnAddZanaMod_Click(object sender, EventArgs e) {
            ZanaModForm zmForm = new ZanaModForm();
            zmForm.ShowDialog();
            ZanaModForm.ZanaMods zanaMod = zmForm.ZanaMod;
            if (zanaMod == ZanaModForm.ZanaMods.NO_MOD) {
                lblZanaMod.Text = "none";
                lblZanaMod.Font = new Font(lblZanaMod.Font, FontStyle.Regular);
                // Todo: clear zana mod on API?
            }
            else {
                String zanaModName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(zanaMod.ToString().ToLower());
                lblZanaMod.Text = zanaModName;
                lblZanaMod.Font = new Font(lblZanaMod.Font, FontStyle.Bold);

                PoMAPIResponse response = API.AddZanaMod(zanaModName.ToLower());
                if (response.RequestSucceeded()) {
                    UpdateUIWithResponse(response);
                    SystemSounds.Exclamation.Play();
                }
                else {
                    ShowError("Could not add Zana mod. Please try again.");
                }

            }
        }

        private void btnSettings_Click(object sender, EventArgs e) {
            ShowSettings();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            try {
                if (!Directory.Exists(Directory.GetParent(CONFIG_PATH).FullName)) {
                    Directory.CreateDirectory(Directory.GetParent(CONFIG_PATH).FullName);
                }

                File.WriteAllText(CONFIG_PATH, JsonConvert.SerializeObject(config));
            }
            catch (Exception) {
                // Todo: log?
            }

#if WEBSERVER_ENABLED
            WS.Stop();
            WebServer.RemoveProgramFromFirewall();
#endif

#if LOGWATCHER_ENABLED
            watcher.StopWatching();
#endif
        }
        #endregion

        #region FORM_FUNCTIONALITY
        private void ReadConfigIntoUI() {
            try {
                if (File.Exists(OLD_CONFIG_PATH)) {
                    if (!Directory.Exists(Directory.GetParent(CONFIG_PATH).FullName)) {
                        Directory.CreateDirectory(Directory.GetParent(CONFIG_PATH).FullName);
                    }
                    File.Copy(OLD_CONFIG_PATH, CONFIG_PATH, true);
                    File.Delete(OLD_CONFIG_PATH);
                }
            }
            catch (Exception) {
                MessageBox.Show("Could not move the old configuration file to the new location.\n\nThe application will not load correctly. " +
                    "Please restart the application.\n\nIf this does not work, please remove the file \"pathofmaps_config.json\" from this current folder.");
                // Todo: log?
            }

            try {
                config = JsonConvert.DeserializeObject<ConfigObj>(File.ReadAllText(CONFIG_PATH));
                this.TopMost = config.always_topmost;
            }
            catch (Exception) {
                // Todo: log?
            }
        }

        private void ResetMapOnUI() {
            picMapIcon.ImageLocation = "";
            lblMapTitle.Text = "#MAP";
            lblMapTitle.ForeColor = Color.White;
            lblMapIIQ.Text = "IIQ: %";
            lblMapIIR.Text = "IIR: %";
            lblMapPackSize.Text = "PackSize: %";
            lblMapQuality.Text = "Quality: %";
            lblMapCorrupted.Text = "no";
            lblMapCorrupted.Font = new Font(lblMapCorrupted.Font, FontStyle.Regular);
            lblZanaMod.Text = "none";
            lblZanaMod.Font = new Font(lblZanaMod.Font, FontStyle.Regular);
            lblMaster.Text = "none";
            lblMaster.Font = new Font(lblMaster.Font, FontStyle.Regular);
            lstMapMods.Items.Clear();
            lstItemsDropped.Items.Clear();
        }

        private void ShowSettings() {
            settingsForm.ShowDialog();
            this.TopMost = config.always_topmost;
        }

        private void UpdateUIWithResponse(PoMAPIResponse response) {
            if (response.map.state.ToLower() == "closed" || response.map.state.ToLower() == "aborted") {
                ResetMapOnUI();
            }
            else {
                lblMapTitle.Text = response.map.display_name;
                lblMapTitle.ForeColor = Color.White;
                if (response.map.rarity.ToLower() == "magic") {
                    lblMapTitle.ForeColor = Color.FromArgb(47, 175, 244);
                }
                else if (response.map.rarity.ToLower() == "rare") {
                    lblMapTitle.ForeColor = Color.FromArgb(254, 254, 118);
                }
                else if (response.map.rarity.ToLower() == "unique") {
                    lblMapTitle.ForeColor = Color.FromArgb(170, 92, 27);
                }

                lblMapIIQ.Text = "IIQ: " + response.map.item_quantity.ToString() + "%";
                lblMapIIR.Text = "IIR: " + response.map.item_rarity.ToString() + "%";
                lblMapPackSize.Text = "PackSize: " + response.map.monster_pack_size.ToString() + "%";
                lblMapQuality.Text = "Quality:" + response.map.quality.ToString() + "%";

                if (response.map.corrupted) {
                    lblMapCorrupted.Text = "yes";
                    lblMapCorrupted.Font = new Font(lblMapCorrupted.Font, FontStyle.Bold);
                }
                else {
                    lblMapCorrupted.Text = "no";
                    lblMapCorrupted.Font = new Font(lblMapCorrupted.Font, FontStyle.Regular);
                }

                if (String.IsNullOrWhiteSpace(response.map.crafted_mod)) {
                    lblZanaMod.Text = "none";
                    lblZanaMod.Font = new Font(lblZanaMod.Font, FontStyle.Regular);
                }
                else {
                    lblZanaMod.Text = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(response.map.crafted_mod.ToLower());
                    lblZanaMod.Font = new Font(lblZanaMod.Font, FontStyle.Bold);
                }

                if (String.IsNullOrWhiteSpace(response.map.master_name)) {
                    lblMaster.Text = "none";
                    lblMaster.Font = new Font(lblMaster.Font, FontStyle.Regular);
                }
                else {
                    lblMaster.Text = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(response.map.master_name.ToLower());
                    lblMaster.Font = new Font(lblMaster.Font, FontStyle.Bold);
                }

                picMapIcon.ImageLocation = response.map.image_url;

                lstMapMods.BeginUpdate();
                lstMapMods.Items.Clear();
                lstMapMods.Items.AddRange(response.map.mods.ToArray());
                lstMapMods.EndUpdate();

                lstItemsDropped.BeginUpdate();
                lstItemsDropped.Items.Clear();
                /*lstItemsDropped.Items.AddRange(response.map.drops.Select(drop => {
                    return "Tier " + drop + " Map";
                }).ToArray());*/
                lstItemsDropped.Items.AddRange(response.map.items.Select(item => {
                    string ret = "";
                    if (item.stackable && item.stack_size > 0) {
                        ret += item.stack_size + "x ";
                    }
                    if (String.IsNullOrWhiteSpace(item.name)) {
                        ret += item.base_name;
                    }
                    else {
                        ret += item.name + " " + item.base_name;
                    }
                    return ret;
                }).ToArray());
                lstItemsDropped.EndUpdate();
            }
        }

        private void ShowError(string msg) {
            lblStatus.Text = msg;
            lblStatus.Show();

            var t = new System.Windows.Forms.Timer() { Interval = 5000 };
            t.Tick += (s, e) => { lblStatus.Hide(); t.Stop(); };
            t.Start();
        }
        #endregion FORM_FUNCTIONALITY

        #region MAIN_FUNCTIONALITY
        /// <summary>
        /// Starts a map by copying the clipboard and sending it to the PoM API.
        /// This function expects to be ran on another thread.
        /// </summary>
        private void StartMap() {
            string mapData = PoMUtils.GetNewClipboardContent();
            if (!String.IsNullOrEmpty(mapData) && !String.IsNullOrWhiteSpace(mapData)) {
                PoMAPIResponse response = API.StartMap(mapData);
                if (response.RequestSucceeded()) {
                    Invoke((MethodInvoker)delegate {
                        UpdateUIWithResponse(response);
                        SystemSounds.Exclamation.Play();
                    });
                }
                else {
                    Invoke((MethodInvoker)delegate {
                        ShowError("Could not start map. Please try again.");
                    });
                }
            }
        }

        /// <summary>
        /// Logs a drop by copying the clipboard and sending it to the PoM API.
        /// This function expects to be ran on another thread.
        /// </summary>
        private void LogDrop() {
            string itemData = PoMUtils.GetNewClipboardContent();
            if (!String.IsNullOrEmpty(itemData) && !String.IsNullOrWhiteSpace(itemData)) {
                PoMAPIResponse response = API.LogDrop(itemData);
                if (response.RequestSucceeded()) {
                    Invoke((MethodInvoker)delegate {
                        UpdateUIWithResponse(response);
                        SystemSounds.Exclamation.Play();
                    });
                }
                else {
                    Invoke((MethodInvoker)delegate {
                        ShowError("Could not log drop. Please try again.");
                    });
                }
            }
        }

        /// <summary>
        /// Ends the currently running map by sending the end command to the PoM API.
        /// This function expects to be ran on another thread.
        /// </summary>
        private void EndMap() {
            PoMAPIResponse response = API.EndMap();
            if (response.RequestSucceeded()) {
                Invoke((MethodInvoker)delegate {
                    UpdateUIWithResponse(response);
                    SystemSounds.Exclamation.Play();
                });
            }
            else {
                Invoke((MethodInvoker)delegate {
                    ShowError("Could not end map. Please try again.");
                });
            }
        }
        #endregion MAIN_FUNCTIONALITY
    }
}
